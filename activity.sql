1. SELECT * FROM artists WHERE name like "%D%";
2. SELECT * FROM songs WHERE length < 230;
3. SELECT albums.name, songs.title, songs.length FROM albums JOIN songs ON songs.album_id = albums.id;
4. SELECT * FROM artists JOIN albums ON albums.artist_id = artists.id WHERE albums.name LIKE "%A%";
5. SELECT * FROM albums ORDER BY name DESC LIMIT 4;
6. SELECT * FROM albums JOIN songs ON songs.album_id = albums.id ORDER BY albums.name DESC, songs.title ASC;

